import asyncio
import time
from selenium.webdriver.common.action_chains import ActionChains
from seleniumwire import webdriver
import json
import aiohttp

def get_tasks(session):
    url = 'https://www.binance.com/bapi/nft/v1/private/nft/mystery-box/purchase'
    tasks = []

    for i in range(0, requestsNumber):
        tasks.append(asyncio.create_task(session.post(url, data=json.dumps(requests_payload), ssl=False)))

    # tasks.extend([asyncio.create_task(session.post(url, data = json.dumps(js), ssl=False))] * 100)
    print(len(tasks))
    return tasks


async def get_symbols(headers):
    async with aiohttp.ClientSession(headers=headers) as session:
        tasks = get_tasks(session)
        print(time.time())
        responses = await asyncio.gather(*tasks)
        for response in responses:
            results.append(await response.text())


def startSsc(headers):
    try:
        return asyncio.get_event_loop().run_until_complete(get_symbols(headers))
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            return asyncio.get_event_loop().run_until_complete(get_symbols(headers))

def sendToServ(headers):
    if not headers:
        print('ERROR: no headers')
        raise SystemExit(1)
    try:
        asyncio.run(main.send_buy_request(
            headers['x-nft-checkbot-token'],
            headers['x-nft-checkbot-sitekey'],
            headers['x-trace-id'],
            headers['x-ui-request-trace'],
            headers['cookie'],
            headers['csrftoken'],
            headers['device-info'],
            headers['user-agent'],
            time.time() + 5
        ))
    except:
        print('Failed to send info to the server')


while True:
    ts = time.time()
    if saleTime>ts:
        print(f'{saleTime-ts} - осталось секунд')
    if saleTime-ts < 13.0:
        break

async def send_buy_request(
        x_checkbot_sitekey,
        x_checkbot_token,
        x_trace_id,
        x_ui_request_trace,
        cookie,
        csrf_token,
        device_info,
        user_agent,
        begin_date
):
    session = aiohttp.client.ClientSession()
    response = await session.post(API_URL,
                                  data=json.dumps({
                                      'x_checkbot_sitekey': x_checkbot_sitekey,
                                      'x_checkbot_token': x_checkbot_token,
                                      'x_trace_id': x_trace_id,
                                      'x_ui_request_trace': x_ui_request_trace,
                                      'cookie': cookie,
                                      'csrf_token': csrf_token,
                                      'user_agent': user_agent,
                                      'device_info': device_info,
                                      'begin_date': begin_date,
                                      **cfg.get_values()
                                  }))
    if response.status != 200:
        raise UserWarning(f'failed to create a buy event: code={response.status}; '
                          f'desc={await response.text()}')

    print('[SUCCESSFUL] Successfully created a buy event')
