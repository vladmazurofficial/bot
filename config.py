from json import dumps

user_token = 'user token here' # future protection

proxy = None # proxy
requests_payload = {"number":1,"productId":156730888620046336} # for buying boxes
product_id = 12577260 # for auction
saleTime = 1637233200-1 # sale time in unix
requestsNumber = 1000 # number of requests

def get_values():
    return {
        'user_token': user_token,
        'proxy': proxy,
        'requests_payload': requests_payload if isinstance(requests_payload, str)
                                             else dumps(requests_payload),  # noqa: E131
        'product_id': product_id
    }
